module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-enum': [1, 'always', [
        'README', 
        'checkstyle', 
        'config', 
        'deps', 
        'gitlab', 
        'husky', 
        'maven', 
        'package', 
        'ossrh']
    ],
  },
};
