package com.anothercaffeinatedday.learn.security.jwt;

import com.anothercaffeinatedday.learn.config.SecurityConfiguration;
import com.anothercaffeinatedday.learn.config.SecurityJwtConfiguration;
import com.anothercaffeinatedday.learn.config.WebConfigurer;
import com.anothercaffeinatedday.learn.management.SecurityMetersService;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;
import tech.jhipster.config.JHipsterProperties;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(
    classes = {
        JHipsterProperties.class,
        WebConfigurer.class,
        SecurityConfiguration.class,
        SecurityJwtConfiguration.class,
        SecurityMetersService.class,
        JwtAuthenticationTestUtils.class,
    }
)
public @interface AuthenticationIntegrationTest {
}
