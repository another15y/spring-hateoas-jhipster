package com.anothercaffeinatedday.learn.web.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.anothercaffeinatedday.learn.domain.Employee;
import com.anothercaffeinatedday.learn.repository.EmployeeRepository;
import com.anothercaffeinatedday.learn.web.rest.errors.BadRequestAlertException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * REST controller for managing {@link com.anothercaffeinatedday.learn.domain.Employee}.
 */
@RestController
@RequestMapping("/api/employees")
@Transactional
public class EmployeeResource {

  private static final Logger LOG = LoggerFactory.getLogger(EmployeeResource.class);

  private static final String ENTITY_NAME = "hateoasEmployee";

  @Value("${jhipster.clientApp.name}")
  private String applicationName;

  private final EmployeeRepository employeeRepository;
  private final EmployeeModelAssembler assembler;

  public EmployeeResource(EmployeeRepository employeeRepository, EmployeeModelAssembler assembler) {
    this.employeeRepository = employeeRepository;
    this.assembler = assembler;
  }

  /**
   * {@code POST  /employees} : Create a new employee.
   *
   * @param employee the employee to create.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new employee, or with status
   *         {@code 400 (Bad Request)} if the employee has already an ID.
   *
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("")
  public ResponseEntity<?> createEmployee(@RequestBody Employee employee) throws URISyntaxException {
    LOG.debug("REST request to save Employee : {}", employee);
    if (employee.getId() != null) {
      throw new BadRequestAlertException("A new employee cannot already have an ID", ENTITY_NAME, "idexists");
    }
    EntityModel<Employee> model = assembler.toModel(employeeRepository.save(employee));
    return ResponseEntity.created(model.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(model);
  }

  /**
   * {@code PUT  /employees/:id} : Updates an existing employee.
   *
   * @param id       the id of the employee to save.
   * @param employee the employee to update.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employee, or with status
   *         {@code 400 (Bad Request)} if the employee is not valid, or with status {@code 500 (Internal Server Error)}
   *         if the employee couldn't be updated.
   *
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/{id}")
  public ResponseEntity<?> updateEmployee(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody Employee employee
  ) throws URISyntaxException {
    LOG.debug("REST request to update Employee : {}, {}", id, employee);
    if (employee.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, employee.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!employeeRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    employee = employeeRepository.save(employee);

    EntityModel<Employee> model = assembler.toModel(employee);

    return ResponseEntity.created(model.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(model);
  }

  /**
   * {@code PATCH  /employees/:id} : Partial updates given fields of an existing employee, field will ignore if it is
   * null
   *
   * @param id       the id of the employee to save.
   * @param employee the employee to update.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employee, or with status
   *         {@code 400 (Bad Request)} if the employee is not valid, or with status {@code 404 (Not Found)} if the
   *         employee is not found, or with status {@code 500 (Internal Server Error)} if the employee couldn't be
   *         updated.
   *
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
  public ResponseEntity<?> partialUpdateEmployee(
    @PathVariable(value = "id", required = false) final Long id,
    @RequestBody Employee employee
  ) throws URISyntaxException {
    LOG.debug("REST request to partial update Employee partially : {}, {}", id, employee);
    if (employee.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, employee.getId())) {
      throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
    }

    if (!employeeRepository.existsById(id)) {
      throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
    }

    Optional<Employee> result = employeeRepository
      .findById(employee.getId())
      .map(existingEmployee -> {
        if (employee.getRole() != null) {
          existingEmployee.setRole(employee.getRole());
        }
        if (employee.getFirstName() != null) {
          existingEmployee.setFirstName(employee.getFirstName());
        }
        if (employee.getLastName() != null) {
          existingEmployee.setLastName(employee.getLastName());
        }

        return existingEmployee;
      })
      .map(employeeRepository::save);

    EntityModel model = assembler.toModel(result.orElseThrow());
    return ResponseEntity.ok().body(model);
  }

  /**
   * {@code GET  /employees} : get all the employees.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of employees in body.
   */
  @GetMapping("")
  public CollectionModel<EntityModel<Employee>> getAllEmployees() {
    LOG.debug("REST request to get all Employees");
    List<EntityModel<Employee>> employees = employeeRepository
      .findAll()
      .stream()
      .map(assembler::toModel)
      .collect(Collectors.toList());
    return CollectionModel.of(employees, linkTo(methodOn(EmployeeResource.class).getAllEmployees()).withSelfRel());
  }

  /**
   * {@code GET  /employees/:id} : get the "id" employee.
   *
   * @param id the id of the employee to retrieve.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the employee, or with status
   *         {@code 404 (Not Found)}.
   */
  @GetMapping("/{id}")
  public EntityModel<Employee> getEmployee(@PathVariable("id") Long id) {
    LOG.debug("REST request to get Employee : {}", id);
    Employee employee = employeeRepository
      .findById(id)
      .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found."));
    return assembler.toModel(employee);
  }

  /**
   * {@code DELETE  /employees/:id} : delete the "id" employee.
   *
   * @param id the id of the employee to delete.
   *
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id) {
    LOG.debug("REST request to delete Employee : {}", id);
    employeeRepository.deleteById(id);
    return ResponseEntity.noContent().build();
  }
}
