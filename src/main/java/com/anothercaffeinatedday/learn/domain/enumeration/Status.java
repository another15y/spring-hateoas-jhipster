package com.anothercaffeinatedday.learn.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
  IN_PROGRESS,
  COMPLETED,
  CANCELLED,
}
